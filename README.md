# Personal Research Website

## Contents

This is my personal research website. It is based on the ArtCore template (see below).

## Template: [ArtCore](http://www.templatemo.com/tm-423-artcore)

## Distributor: [Templatemo](http://www.templatemo.com/)

## Configuration

- Default
- GitLab CI: [`.gitlab-ci.yml`](https://gitlab.com/html-themes/artcore/blob/master/.gitlab-ci.yml)

## Links

- [Template Wiki](https://gitlab.com/html-themes/artcore/wikis/home)